$(document).ready(function(e) {
    let paymentForm = document.querySelector('[data-sysea-payment-display="true"]');
    let shippingForm = document.querySelector('[data-sysea-shipping-display="true"]');
    let compatibilityMode = document.querySelector('form[data-sysea-autoconfirm="true"]');

    if(compatibilityMode) {

        if(paymentForm) {
            let radios = paymentForm.querySelectorAll('.payment-methods input[type="radio"]');

            radios.forEach(element => element.addEventListener('click', function(e) {
                paymentForm.submit();
            }));
        }

        if(shippingForm) {
            let radios = shippingForm.querySelectorAll('.confirm-shipping-fields input[type="radio"]');

            radios.forEach(element => element.addEventListener('click', function(e) {
                shippingForm.submit();
            }));
        }

    } else {

        let confirmPaymentForm = document.querySelector('#sysea-payment-confirm');
        let defaultSelection = document.querySelector('.payment-methods > .payment-method [type="radio"]:checked');
        let defaultRadioId = "";
        if (defaultSelection) {
            if (defaultSelection.id !== undefined) {
                defaultRadioId = defaultSelection.id;
            }
        }

        if (paymentForm) {
            let radios = paymentForm.querySelectorAll('.payment-methods input[type="radio"]');
            radios.forEach(element => element.addEventListener('click', function (e) {
                if (element.id != defaultRadioId) {
                    let parentElement = element.parentElement;
                    let alreadySelected = parentElement.querySelector('#sysea-payment-confirm');
                    if (!alreadySelected) {
                        parentElement.insertAdjacentElement('beforeend', confirmPaymentForm);
                        confirmPaymentForm = document.querySelector('#sysea-payment-confirm');
                    }
                } else {
                    let confirmPaymentForm = document.querySelector('#sysea-payment-confirm');
                    paymentForm.insertAdjacentElement('beforeend', confirmPaymentForm);
                }
            }));
        }

        if (shippingForm) {
            let radios = shippingForm.querySelectorAll('.confirm-shipping-fields input[type="radio"]');

            radios.forEach(element => element.addEventListener('click', function (e) {
                shippingForm.submit();
            }));
        }

    }
});